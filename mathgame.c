/* Math game on C */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

long show_problem(int difficult)
{
    int oper;
    long a, b, rightans;
    a = (rand() % difficult) + 1;
    b = (rand() % difficult) + 1;
    oper = rand() % 2;
    printf("%ld %c %ld = ?\n", a, oper ? '+' : '-', b);
    if(oper) {
        rightans = a + b;
    } else {
        rightans = a - b;
    }
    return rightans;
}

int main()
{
    int difficult = 2, count_right = 0;
    long ans, rightans;
    srand(time(NULL));
    for(;;) {
        rightans = show_problem(difficult);
        printf("Your answer: ");
        scanf("%ld", &ans);
        if(ans == rightans) {
            count_right++;
            difficult *= 2;
        } else {
            printf("Wrong, right answer is %ld\n", rightans);
            printf("Number of your right answers is %d\n", count_right);
            break;
        }
    }
    return 0;
}
